# Unsupervised Auto-Illustration

This program is designed to auto-illustrate text by means of image retrieval.

The included web scraper is designed to download tourist information based on the user's input locations. Another scraper or text files should be used to auto-illustrate other texts.



## Getting started
The program was written in Python 3.6. Keras-related errors may occur using later versions.

The scripts require several libraries, in addition to commonly used libraries (```json```,```urllibread```, ```BeautifulSoup```, ```numpy```).


### Libraries
NLP/Semantic web
* [NLTK](http://www.nltk.org/) - after installing NLTK, download WordNet data with ```$ python -m nltk.downloader 'wordnet'``` and Open Multilingual WordNet with ```$ python -m nltk.downloader 'omw'```
* [BabelPy](https://github.com/fbngrm/babelpy) - Python client for the Babelfy API
* [SpaCy](https://spacy.io/) - after installing SpaCy, download the French language model with ```$ python -m spacy download fr ```
* [SPARQLWrapper](https://rdflib.github.io/sparqlwrapper/) - wrapper for SPARQL endpoints

Machine learning
* [sci-kit](http://scikit-learn.org/) - algorithm implementation
* [Keras](https://keras.io/) - algorithm implementation using the MobileNet model available for TensorFlow. Requires the Pillow library ( ```$ pip install Pillow```).

Images
* [flickrapi](https://stuvel.eu/flickrapi) - Wrapper for Flickr API


### APIs
The program requires the user to have valid API keys for the [Babelfy](http://babelfy.org/guide) and [Flickr APIs](https://www.flickr.com/services/api/), which should be saved in ```apis.json```.



## Running the program

There are four components of the program, run with four scripts:
1. ```code/scraper.py```: Scraping. Creates a _text_ directory with scraped text files.
2. ```code/main.py```: Text analys. Creates a _results_ directory, containing subdirectories for each analyzed sentence. Returns and creates a json file with the text's entity information.
3. ```code/autoimage.py```: 
4. ```code/evaluate.py```: Evaluates the retrieved images. Updates the json file (created in step 2) with best images.
5. ```code/display.py```: Displays the images in html (_optional_).  Creates a main display for the text (_keyword.html_) saved in the _results_ directory, and multiple html files for each illustrated sentence (_0.html_, _1.html_, and so on), saved in the text's directory (_keyword/place_).

### 1 Scraping websites
Running ```scraper.py``` will prompt the user to list places for which info will be retrieved and auto-illustrated.

```
$ python scraper.py
```

```
Enter a location (separate different locations with commas): rome, kansas
"rome": Text retrieved and saved as texts/rome.txt
"kansas": Text retrieved and saved as texts/kansas.txt
```

The script will create a directory of text documents.


### 2 Text analysis

Running  ```main.py``` will analyze the text for which images will be retrieved.

```
$ python main.py dir/with/text/docs dir/with/apikeys.json LANG
```

Currently, the program only works with French texts, recognized with ```FR```. An example of running the program: ```$ python code/main.py /Users/me/files/text /Users/me/files/apis.json FR```

The following will be created:
* a  _/results_ directory
* sub-directories corresponding to the number of sentences in the analyzed text file (_/0_, _/1_, and so on)
* a json file containing the text's recognized entity and concept information



### 3 Image retrieval

Running  ```autoimage.py```  queries Flickr for images and saves the Flickr and Wikimedia images to the sub-directories created in step 2.

You must enter the location of the generated json file (step 2), the json file with the api keys, and the maximum number of images to retrieve.

```
$ python autoimage.py dir/with/results/keyword/keyword.json apikeys.json #
```

For example: ```$ python code/autoimage.py /Users/me/files/results/texas/texas.json /Users/me/files/apikeys.json 5```

The script updates the json file with the locally saved images, in addition to downloading the images.




### 4 Image evaluation
Running ```evaluation.py``` checks the contents of the retrieved images to evaluate which are matches to the original text.

```
$ python evaluation.py dir/with/results/keyword/keyword.json
```

The json file in each subdirectory (created with ```main.py```) is updated with a *best* item, which includes an ordered list of the retrieved images.






### 5 Displaying (optional)
Running ```display.py``` creates html files that display the json file's contens.

```
$ python display.py dir/with/results/keyword/keyword.json
```

This generates multiple html files.

* Main display: _keyword.html_, displays all sentences and highlights identified entities. Green highlight indicates entities for which verified images (checked for in ```evaluation.py```) have been retrieved. These images are displayed. Yellow highlights indicates entities that have unverified photos.

* Sentence displays: These pages are linked to in the main display. They include all entities (yellow and green) and photos retrieved.




## Resources
TODO: cite most pertinent papers




## Authors

Paula Pawlowski <ppwlwski@gmail.com> / <paula.pawlowski@inria.fr>

Supervision provided by Pierre Kornprobst and Elena Cabrio.






