'''
To run: 
$ python scraper.py

'''

import requests, os, json, re, sys
import urllib.request
from bs4 import BeautifulSoup


'''
Retrieves text from the routard site
'''
def get_routard(url, place, lang= 'fr'):
	#Get search results
	page_response = requests.get(url, timeout=5)
	soup = BeautifulSoup(page_response.content, "html.parser")
	results= soup.find_all(class_="r")
	links = []
	altlinks = []
	for r in results[:4]:
		#Best result
		link = re.findall(r"https\:\/\/www\.routard\.com\/guide\/code\w*\/\w*\.html*", str(r))
		if link:
			links.append(link[0])
		#Alternative results
		alt = re.findall(r"https\:\/\/www\.routard\.com\/guide\w*\/\w*[-]?\w*\.html*", str(r))
		if alt:
			altlinks.append(alt[0])

	#Save texts
	text = None

	if links:
		url = links[0]
		page_response = requests.get(url, timeout=5)
		class_ = 'home-dest-desc'

	elif altlinks:
		url = altlinks[0]
		page_response = requests.get(url, timeout=5)
		soup = BeautifulSoup(page_response.content, "html.parser")
		class_ = 'lieu-intro'

	soup = BeautifulSoup(page_response.content, "html.parser")
	desc = soup.find(class_= class_)
	if desc:
		text = desc.text.strip()

	#Write travel text to file	
	if text:
		if not os.path.exists('texts'):
			os.makedirs('texts')

		text_file = 'texts/' + place.replace('%20', '') + '.txt'
		with open(text_file, "w") as myfile:
			myfile.write(text)

		print('"' + place.replace('%20', ' ') + '": Text retrieved and saved as ' + text_file)
	
	else:
		print('"' + place.replace('%20', ' ') + '": No text retrieved. Enter a more specific place.')
		


if __name__ == '__main__':

	places = input("Enter a location (separate different locations with commas): ")
	places = places.lower().split(',')

	for place in places:
		place = place.strip()
		if ' ' in place:
			place = place.replace(' ', '%20')

		url = "https://www.google.com/search?hl=en&q=guide%20routard%20" 
		url = url + place

		get_routard(url, place, lang= 'fr')

