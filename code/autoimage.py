'''
To run:

$ python autoimage.py files/results/keyword/keyword.json file/with/apikeys.json
'''

import json, os, re, sys 
import urllib.request
from collections import defaultdict
import unicodedata

from babelpy.babelfy import BabelfyClient
import flickrapi
from nltk.corpus import wordnet as wn

import spacy

#Local
import autotext
import autosyn


'''
Function gets NE images out of babel.
'''
def get_ne_imgs(entity, apis, path_n, n_images= 5):	
	BAB_API_KEY = apis['babel']['key']

	if 'babelSynsetID' in entity:
		url = 'https://babelnet.io/v3/getSynset?id=' + entity['babelSynsetID'] + '&key=' + BAB_API_KEY

	else:
		#print('No NE image retrieval in Babel: *NO TYPE*')
		return []
		
	#Access Synset info
	with urllib.request.urlopen(url) as url:
		data = json.loads(url.read().decode())

	#Get images from the Synset info
	imageURLs = []
	imageFiles = []
	if 'synsetType' in data.keys():
		if data['synsetType'] == 'NAMED_ENTITY' and len(data['images']) > 0 :
			c = 0 #the image number
			for image in data['images'][:n_images]:
				if not image['url'].endswith('svg') and not image['url'].endswith('webm'): #ignore these imagetypes
					
					imageFile = path_n + '/' + entity['text'].replace(' ', '') + '_babel' + str(c) + '.' + image['url'].split('.')[-1]
					imageFiles.append(imageFile)
					
					imageURLs.append(image['url'])
					c += 1	#update image number
	
	return imageURLs, imageFiles	


'''
Accesses Flickr API to get images.
'''
def get_flickr(jsonfile, apis, n, lang = 'fr'):

	with open(jsonfile, "r+", encoding='utf-8') as f:
		data = json.load(f)
	
	#Accessing the Flickr API
	API_KEY = apis['flickr']['key']
	API_SECRET = apis['flickr']['secret']

	flickr = flickrapi.FlickrAPI(API_KEY, API_SECRET, format='json')

	for sentence in data['sentences']:
		photofolder = data['results'] + '/' + str(sentence['number'])
		
		
		for entity in sentence['entities']:

			for query in entity['query']:
				photosearch = flickr.photos.search(tags= query, per_page= n, extras= 'url_m', content_type= 1, sort= 'relevance', tag_mode= 'all')
				parsed = json.loads(photosearch)

				c = 0 #photocount
				if len(parsed['photos']['photo']) != 0:
					for parse in parsed['photos']['photo']:
						
						#Search for and save photos
						#TODO: check error here
						url = parse['url_m']

						if not url.endswith('svg') and not url.endswith('webm'):

							entity['imageURLs'].append(url)

							imageFile = entity['imageFolder'] + '/' + query.replace(' ', '') + '_flickr' + str(c) + '.' + url.split('.')[-1]
							entity['imageFiles'].append(imageFile)
							
							c += 1 #update photocount

	return data


'''
'''
def download_images(jsonfile, apis):
	with open(jsonfile, "r+", encoding='utf-8') as f:
		data = json.load(f)

	for sentence in data['sentences']:
		for entity in sentence['entities']:
			for n in range(0, len(entity['imageURLs'])):
				urllib.request.urlretrieve(entity['imageURLs'][n], entity['imageFiles'][n])
	
	with open(jsonfile, "w", encoding='utf-8') as f:
		json.dump(data, f, indent=3, ensure_ascii=False)


'''
Retrieve images.
Some entities will have queries (Flickr search) while some won't (Babel search, then Flickr search).

'''
def retrieveimages(jsonfile, apis, n):

	data = get_flickr(jsonfile, apis, n, lang = 'fr')
	
	with open(jsonfile, "w", encoding='utf-8') as f:
		json.dump(data, f, indent=3, ensure_ascii=False)
	
	return jsonfile


'''
Main
'''
if __name__ == '__main__':
	jsonfile = sys.argv[1] #folder with texts
	apikeys = sys.argv[2] #json file
	n = sys.argv[3] #number of photos to retrieve

	with open(apikeys, "r+", encoding='utf-8') as f:
		apis = json.load(f)

	jsonfile = retrieveimages(jsonfile, apis, n)
	jsonfile_withimages = download_images(jsonfile, apis)

