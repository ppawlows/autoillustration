from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.datasets import fetch_20newsgroups
from sklearn.decomposition import NMF, LatentDirichletAllocation

import unicodedata

import spacy
from SPARQLWrapper import SPARQLWrapper, JSON

from bs4 import BeautifulSoup



#Reads text files and returns a list of the documents (each index is the full text string)
def read_docs(text_path):
	document = []

	text_list = []

	with open(text_path, 'r') as fin:
		text = fin.read().strip()	

		#text=unicodedata.normalize('NFKC', text)
		
		fr = spacy.load('fr_core_news_md')
		doc =fr(text)

		for token in doc:
			#print(token.text, token.pos_, token.dep_)
			'''if token.pos_ =='NOUN':
				text.append(token.lemma_.lower())
			elif token.pos_ == 'PROPN':
				text.append(token.lemma_.lower())
			elif token.pos_ == 'ADJ':
				text.append(token.lemma_.lower())
			else:
				pass'''

			text_list.append(token.text)
			#text.append(token.lemma_.lower())
	tokenized_text = ' '.join(text_list)
	
	document.append(tokenized_text)

	return document


#Glues together the top topics
def display_topics(model, feature_names, no_top_words):
	t = None
	for topic_idx, topic in enumerate(model.components_):
		print("Topic %d:" % (topic_idx), " ".join([feature_names[i] for i in topic.argsort()[:-no_top_words - 1:-1]]))
		t = " ".join([feature_names[i] for i in topic.argsort()[:-no_top_words - 1:-1]])

	return t


#Runs the algorithm to find the topics
def candidates(doc):
	sw = ['dans', 'aux', 'le', 'la', 'les', 'des', 'un', 'en', 'une',  'au', 'à', 'il', 'elle', 'et', 'ou', 'que' 'ça', 'est', 'sont', 'a', 'ont', 'et', 'ou', 'qui', 'que', 'se', 'pour', "d'", "l'", 'vous', 'lui', 'nous', 'tu', 'ceux', 'ce', 'cel', 'celle', 'cet', 'cette', 'ces', 'plus', 'beaucoup', 'peu', 'donc', 'ainsi', 'entre', 'si', 'depuis', 'par', 'sa', 'ses', 'son']
	sw_lemma = sw + ['être', 'avoir']

	no_topics = 1
	no_top_words = 5
	no_features = 5000
	
	# Run LDA
	# LDA can only use raw term counts for LDA because it is a probabilistic graphical model
	tf_vectorizer = CountVectorizer(max_df=1, min_df=0, max_features=no_features, stop_words=sw_lemma, ngram_range = (1,3))
	tf = tf_vectorizer.fit_transform(doc)
	tf_feature_names = tf_vectorizer.get_feature_names()

	lda = LatentDirichletAllocation(n_components=no_topics, max_iter=10, learning_method='online', learning_offset=50, learning_decay=0.8, random_state=None).fit(tf)

	#print('LDA')
	candidates= display_topics(lda, tf_feature_names, no_top_words)
	
	return candidates



def ner_keywords(candidates, doc, babel_client):

	#Babelfy sentence (str)
	babel_client.babelfy(candidates)

	score = 0
	keyword = None

	endpoint = SPARQLWrapper("http://dbpedia.org/sparql")

	db =[]
	text = []
	
	for n in range(0, len(babel_client.merged_entities)):
		entity= babel_client.merged_entities[n]
		#print(entity)
		if entity['DBpediaURL'] != '':
			if entity['text'] not in text: #no duplicates
				db.append(entity['DBpediaURL'])
				text.append(entity['text'])

		

				m = """prefix dbo: <http://dbpedia.org/ontology/> select * where { <""" + entity['DBpediaURL'] + """> dbo:abstract ?text . 
				FILTER (lang(?text) = 'fr')}"""

				endpoint.setQuery(m)
				ret = endpoint.query()
				endpoint.setReturnFormat(JSON)
				query = endpoint.query().convert()
				#print('****query', query)
				if len(query['results']['bindings']) >=1 :
					abstract = query['results']['bindings'][0]['text']['value']
				else:
					abstract = 'None'

				#Compare the Wiki abstract to the text
				nlp = spacy.load('fr_core_news_md')
				
				#Basic version
				abstract_spacy = nlp(abstract)
				
				if type(doc) == list:
					doc = doc[0]
				doc_spacy = nlp(doc)

				#Lemmatized version
				"""doc_clean=[]
				for token in doc_spacy:
					doc_clean.append(token.lemma_)
				doc_spacy= ' '.join(doc_clean)
				doc_spacy=nlp(doc_spacy)

				abstract_clean=[]
				for token in abstract_spacy: 
					abstract_clean.append(token.lemma_)
				abstract_spacy= ' '.join(abstract_clean)
				abstract_spacy= nlp(abstract_spacy)"""

				sim = abstract_spacy.similarity(doc_spacy)

				if sim > score:
					score = sim
					keyword = entity['text']
	
	return keyword


