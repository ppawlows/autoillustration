'''
To run:

python display.py location/of/json/file/with/data.json
'''

import requests, os, re, json, sys

from nltk.corpus import wordnet as wn


#css files for all
if os.path.basename(os.path.normpath(os.getcwd())) == 'autoillustration':
	style = os.getcwd() + "/code/css/style.css"
elif os.path.basename(os.path.normpath(os.getcwd())) == 'results':
	style = os.path.dirname(os.getcwd()) + "/code/css/style.css"
elif os.path.basename(os.path.normpath(os.getcwd())) == 'code':
	style = os.getcwd() + "/css/style.css"
else: #just a guess
	style = os.getcwd() + "/css/style.css"


'''
Inserts images onto html pages.
Two types of images are distinguished: filtered "green" images, and unfiltered "yellow" images
Verified images (best==True) have green overlay; unverified ones have yellow overlay.
'''
def insert_image(htmlfile, entity, image, best=True):

	if best == True:
		boxInner = 'boxInnerBest'
		titleBox = 'titleBoxBest'
	elif best == False:
		boxInner = 'boxInnerYellow'
		titleBox = 'titleBoxYellow'
	
	image = '<div class="box"><div class="' + boxInner +'"><a href="'+ image + '" target="_blank"><img src="'+ image +'" width="200" height="200"></a><div class="'+ titleBox +'">' + entity['recognized'] +'</div></div></div>'

	with open(htmlfile, "a", encoding='utf-8') as html:
		html.write(image)	


'''
Inserts titles onto html pages.
Two types of pages are produced: the main page and the sentence pages.
'''
def insert_title(htmlfile, entity, subject, main = True, text=None):
	#Main page displays all sentences
	if main == True:
		with open(htmlfile, "w", encoding='utf-8') as html:
			title = '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0" /><title>' + subject +'</title><link rel="stylesheet" href="'+ style + '" type="text/css"></head><body class="no-touch"><h1><center>'+ subject +'</center></h1><div class="break">.<br></div>'
			html.write(title)	

	#Sentence page displays a sentence and all entities' images
	elif main == False:
		number = re.findall(r'(\w*)\.html', htmlfile)
		number = number[0] #the sentence number
		with open(htmlfile, 'r+') as html:
			title = '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><!-- Enable responsive view on mobile devices --><meta name="viewport" content="width=device-width, initial-scale=1.0" /><title>' + subject + '</title> <!--VARIABE--><link rel="stylesheet" href="' + style + '" type="text/css"><!--VARIABE--><!-- Enable media queries for old IE --><!--[if lt IE 9]><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]--></head><body class="no-touch"><center><h1>Sentence ' + number + '</h1><p> <!--VARIABLE--><div class= "text">' + text +'</div></center><div class="break">.<br></div>'
			content = html.read()
			html.seek(0, 0) #Ensure title is on top of page
			html.write('\n')
			html.write(title.rstrip('\r\n') + '\n' + content)


'''
Close html pages
'''
def end_page(htmlfile):
	with open(htmlfile, "a", encoding='utf-8') as html:
		endpage = '<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.js"></script><script type="text/javascript">$(function(){if ("ontouchstart" in window){$("body").removeClass("no-touch").addClass("touch");$("div.boxInnerBest img").click(function(){$(this).closest(".boxInnerBest").toggleClass("touchFocus");});}});</script></body></html>'
		html.write(endpage)

		
'''
Two types of pages are made: the main page (1) and sentence pages (multiple)
Main page: displays all sentences with highlighted (green= with verified images, yellow= without) entities and verified images only.
Sentence pages: displays all entities and all retrieved images (verified and unverified)
'''
def makepages(jsonfile):
	with open(jsonfile, "r+", encoding='utf-8') as f:
		data = json.load(f)

	#Subject (aka keyword) but stylized
	subject = data['keyword'][0].upper() + data['keyword'][1:]

	#Create and title html file for the main page
	mainfile = data['results'] + '/' + data['keyword'] + '.html'
	insert_title(mainfile, style, subject, main=True)

	#Iterate through sentences
	for sentence in data['sentences']:

		#Create and open sentence page
		sentencefile = data['results'] + '/' + str(sentence['number']) + '.html'
		with open(sentencefile, "w", encoding='utf-8') as html:
			html.write(' ')	

		with open(mainfile, "a", encoding='utf-8') as html:
			html.write('<div class="wrap">') #Start image block

		text = sentence['text']
		
		for entity in sentence['entities']:
			with open(sentencefile, "a", encoding='utf-8') as html:
				html.write('<div class="wrap"><div class= "text"><h2>' + entity['recognized'] + '</h2></div>') #Print entity

			#Distinguish between filtered and unfiltered images

			#Filtered photos
			if entity['best']:
				green_highlight = '<span style="background-color: #62AE5C"><b>' + entity['recognized'] + '</b></span>'
				text = text.replace(entity['recognized'], green_highlight) #highlight entity in green

				#Display best images on the main and sentence files
				maxx = len(entity['best']) - 1 #can change amount
				for i in range(0, maxx):
					insert_image(mainfile, entity, entity['best'][i], best=True)
					insert_image(sentencefile, entity, entity['best'][i], best=True)
					

			#Unfiltered
			elif not entity['best']: 
				yellow_highlight = '<span style="background-color: #F9F476"><b>' + entity['recognized'] + '</b></span>'
				text = text.replace(entity['recognized'], yellow_highlight) #highlight entity in yellow

				#Display unverified images on the sentence page
				maxx = len(entity['imageFiles']) - 1 #can change amount
				for i in range(0, maxx):
					insert_image(sentencefile, entity, entity['imageFiles'][i], best = False)
			
			#Insert breaks on sentence page
			with open(sentencefile, "a", encoding= 'utf-8') as html:
				html.write('</div><div class="break">.<br></div>')

		#Insert breaks on main page
		with open(mainfile, "a", encoding='utf-8') as html:
			endsentence = '</div><div class="text">' + text + '<br><a href="'+ sentencefile +'" target="_blank">{see all}</a></div><div class="break">.<br>.<br></div>'
			html.write(endsentence)
			
		#Title sentence file with highlighted text and close page
		insert_title(sentencefile, style, subject, main = False, text = text)	
		end_page(sentencefile)

	#Close main file
	end_page(mainfile)	


if __name__ == '__main__':
	jsonfile = sys.argv[1] #folder with texts
	makepages(jsonfile)
