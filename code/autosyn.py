'''
Functions are accessed by other programs.
'''

import json, os, re, sys 
import urllib.request
import unicodedata
from itertools import islice

from babelpy.babelfy import BabelfyClient
from nltk.corpus import wordnet as wn

import flickrapi

#Local
import autoimage
import autotext


'''
Get the corresponding synset id of the synset.
:param synset: The synset to extract the id from
:return: The corresponding synset id
'''
def get_synset_id(synset):
	sid = "n{}".format(str(synset.offset()).zfill(8))
	#sid = str(synset)[]
	return sid 


'''
Translates an entity's offset into synset; returns the main synset (str).
'''
def get_synset(entity, apis):
	BAB_API_KEY = apis['babel']['key']

	url= 'https://babelnet.io/v3/getSynset?id=' + entity['babelSynsetID'] + '&key=' + BAB_API_KEY
	
	with urllib.request.urlopen(url) as url:
		data = json.loads(url.read().decode())
	
	synset = None
	
	#Translate offset into sysnet.
	if 'wnOffsets' in data:
		if data['wnOffsets'] != []:
			for wnidx in data['wnOffsets']:
				wnid = re.findall(r'(\d{8}\w)', wnidx['id'])
				syn = wn.of2ss(wnid[0])
				synset = syn.name()
				
	lang = "fra" #use wn.langs() to see list of supported wn langs in nltk
	
	#Check if synset == None because entity is compound; take the first element if so.
	#TODO: expand
	if not synset:
		compound_entity = entity['text'].split(' ')
		if len(compound_entity) > 1:
			simplified_entity = compound_entity[0]
			synset = wn.synsets(simplified_entity, lang = lang)
			if synset:
				synset = synset[0].name()
	
	return synset


'''
Provides the translation of a concept in terms of translation provided by Babel.
Input: entity dictionary
#TODO: account for translation of UD
#currently takes the entity and translates it using WN, but UD terms aren't accounted for in EN
'''
def syn_translation(entity, apis, target_lang = 'eng'):

	BAB_API_KEY = apis['babel']['key']

	lang_disambiguation = {'eng' : ['eng', 'english', 'en', 'eng', 'an', 'anglais'], 'spa' : ['spa', 'spanish', 'spn', 'sp', 'es', 'esp'], 'fra': ['fra', 'french', 'français', 'francais', 'fr', 'fre']} #see full list of valid language codes in wn.langs()

	for lang in lang_disambiguation:
		if target_lang.lower() in lang_disambiguation[lang]:
			target_lang = lang

	translation = None

	#For concept/entity translation
	if type(entity) == dict:
		url = 'https://babelnet.io/v5/getSynset?id=' + entity['babelSynsetID'] + '&key=' + BAB_API_KEY
		
		with urllib.request.urlopen(url) as url:
			data = json.loads(url.read().decode())
			#TODO: EN translation


	#For string translation; assumes 1-word keyword.
	elif type(entity) == str:
		if ' ' in entity:
			entity = entity.replace(' ', '_')

		translation_ss = wn.synsets(entity) 
		translation_ss = str(translation_ss[0])[8:-2]
		translation = wn.synset(translation_ss).lemma_names(target_lang)
		if translation:
			translation = translation[0]
		
	#Clean up underscores for cleaner queries
	if translation:
		translation = translation.lower()
		if  '_' in translation:
			translation = translation.replace('_', ' ')
	else:
		translation = entity['text'].lower() #THIS WAS THEBUG

	return translation
		

'''
'''
def check_noun(entity, apis):

	BAB_API_KEY = apis['babel']['key']

	if 'babelSynsetID' in entity:
		url = 'https://babelnet.io/v3/getSynset?id=' + entity['babelSynsetID'] + '&key=' + BAB_API_KEY
	else:
		return False

	with urllib.request.urlopen(url) as url:
		data = json.loads(url.read().decode())
	
	#First check that compound entities aren't nouns
	if 'wnOffsets' in data:
		if data['wnOffsets']:
			for wnidx in data['wnOffsets']:
				if wnidx['pos'] != 'NOUN':
					return False
				else:
					return True

		elif not data['wnOffsets']:
			if 'senses' in data and data['senses']:
				for sense in data['senses']:
					if sense['pos'] != 'NOUN':
						return False
					else:
						return True



'''
Checks if a concept is abstract or not by going through the WordNet hierarchy.
'''
def check_abstract(entity, apis):
	BAB_API_KEY = apis['babel']['key']


	if 'babelSynsetID' in entity:
		url = 'https://babelnet.io/v3/getSynset?id=' + entity['babelSynsetID'] + '&key=' + BAB_API_KEY
	else:
		return True #default to abstract if an entity doesn't have any info

	with urllib.request.urlopen(url) as url:
		data = json.loads(url.read().decode())
	
	link = entity['BabelNetURL']

	#First check that compound entities aren't nouns
	if 'wnOffsets' in data and data['wnOffsets']:
		for wnidx in data['wnOffsets']:
			wnid = re.findall(r'(\d{8}\w)', wnidx['id'])
			syn = wn.of2ss(wnid[0])

			paths = syn.hypernym_paths()
			for path in paths:
				nodes = [syn.name().split('.')[0] for syn in path]
				
			if 'abstraction' in nodes:
				if 'communication' in nodes:
					return False
				else:
					#print('Rejected: abstraction')
					return True
			else:
				#print('Accepted: Not an abstraction')
				return False
	else:
		return True #default to abstract if an entity doesn't have any info


'''
#Function distinguishes between named entities and concepts. 
'''
def check_ne(entity, apis):

	BAB_API_KEY = apis['babel']['key']

	if 'babelSynsetID' in entity:
		url = 'https://babelnet.io/v3/getSynset?id=' + entity['babelSynsetID'] + '&key=' + BAB_API_KEY
		
		with urllib.request.urlopen(url) as url:
			data = json.loads(url.read().decode())
		
		if 'synsetType' not in data:
			return False
		
		elif data['synsetType'] == 'NAMED_ENTITY':
			#print('Type: ' + data['synsetType'])
			return True
		
		else: 
			#print('Type: ' + data['synsetType'])
			return False
	else:
		return False


'''
Checks the domain of an entity. (no longer used)
Retained just in case.
'''
def check_domain(entity, apis):
	BAB_API_KEY = apis['babel']['key']

	url = 'https://babelnet.io/v3/getSynset?id=' + entity['babelSynsetID'] + '&key=' + BAB_API_KEY
	
	with urllib.request.urlopen(url) as url:
		data = json.loads(url.read().decode())

	#domains to ignore
	ignore_domains = ['PHYSICS_AND_ASTRONOMY', 'ENGINEERING_AND_TECHNOLOGY', 'PHILOSOPHY_AND_PSYCHOLOGY', 'LAW_AND_CRIME', 'CHEMISTRY_AND_MINERALOGY', 'MATHEMATICS']

	if 'domains' not in data:
		write = 'No domains in data'
		return False

	elif data['domains'] == None: 
		#print('Concept, len(domains) == 0 : ignore concept')
		return False

	elif len(data['domains']) > 1:
		#print('Concept, len(domains) > 1 : ignore concept')
		return False

	else:
		for domain in data['domains']:
			if domain in ignore_domains:
				write = 'Bad domain: ' + str(domain)
				return False
			else:
				write = 'Good domain: ' + str(domain)
				return True

