'''
Functions are accessed by other programs.
'''

import json, os, re, sys 
import urllib.request
import unicodedata
from itertools import islice

from babelpy.babelfy import BabelfyClient
from nltk.corpus import wordnet as wn
import spacy

import flickrapi

#Local
import autoimage
import autosyn


'''
Potential changes:
- lang is currently only cofigured to 'fr' but can be changed
'''
def spacy_tok(filepath, subfolder, lang, babel_client, jsonfile, apis):
	if lang.isupper():
		lang = lang.lower()

	#Read the text file
	with open(filepath, 'r') as myfile:
		text = myfile.read().strip()
		text = unicodedata.normalize('NFKC', text)

	#Split the text into sentences.
	text_split = [s.strip() for s in re.split(r"(?<!\.)[.?!](?!\.|\.|\s\»|\s\")", text)]
	
	#Start the spaCy preprocessing. Can update to include other languages.
	if lang == 'fr':
		fr = spacy.load('fr_core_news_md')

	n = 0 #sentence count
	
	#Cleans up potential trailing blanks bc of splitting
	if text_split[-1] == '':
		text_split = text_split[:-1]
	elif text_split[-1] == ' ':
		text_split = text_split[:-1]
	
	for sentence in text_split:
		#We duplicate the spaCy tokenization to feed into Babel. Better recognition of entities and keeps indexes consistent.
		doc = fr(sentence)
		tokenized = []
		for token in doc: #Eliminate extra spaces and dashes as punctuation
			if token.pos_ == 'SPACE':
				pass
			elif token.text == '-' and token.pos_ == 'PUNCT':
				pass
			else:
				tokenized.append(token.text)

		#Join the tokenized sentence so it can be processed
		tokenized_join = ' '.join(tokenized)
		tokenized = [tokenized_join]
		
		for sentence in tokenized:
			#Make a separate folder for every sentence
			path_n = subfolder + '/' + str(n)
			if not os.path.isdir(path_n):
				os.makedirs(path_n, exist_ok = True)
			
			#Get analysis
			doc = fr(sentence)
			spacy_dic = {} #dictionary that will include text features
			for token in doc: 
				spacy_dic[token.idx] = {}
				spacy_dic[token.idx]['text'] = token.text
				spacy_dic[token.idx]['ud'] = token.dep_
				spacy_dic[token.idx]['pos'] = token.pos_

			#TODO: this isn't just the entities list: it's the list of images and everything!!	
			entities_list = bab_entities(spacy_dic, sentence, path_n, babel_client, apis, jsonfile)

			#Update json
			with open(jsonfile, "r+", encoding='utf-8') as f:
				data = json.load(f)
				new_sentence = {'number': n, 'text': sentence, 'entities': entities_list}
				data['sentences'].append(new_sentence)
			with open(jsonfile, "w", encoding='utf-8') as f:
				json.dump(data, f, indent=3, ensure_ascii=False)				
			
		n += 1 #increase sentence count

	return jsonfile


'''
#The window list is a list of tuples [(entity character index, entity UD tag)]. Each tuple represents a word immediately to the left and right of the identified identity.
#The window list will be used to link UD tags to the terms at the character index.
#The ud_sublist list is a list of the UD tags (the same as index 1 of the tuples in window).
#The ud_sublist list will be used to compare to a list of patterns we want to pick up.
'''
def make_ud(start, end, all_merged_entities, spacy_dic):
	window = []
	ud_sublist = []
	for n in range(start, end):
		entity = all_merged_entities[n]
		window.append((entity['start'], spacy_dic[entity['start']]['ud']))
		ud_sublist.append(spacy_dic[entity['start']]['ud'])
	return window, ud_sublist



'''
#TODO: THE UD TRANSLATION WILL GO DOWN HERE SOMEHOW
The function currently only targets UD relations within three index positions.
More relations can be outlined.
'''
def ud(n, all_merged_entities, spacy_dic, queries):
	try:
		for n in range(n, n+1):
			if n+3 in range(0, len(all_merged_entities)) and n != 0:
				window, ud_sublist = make_ud(n-1, n+4, all_merged_entities, spacy_dic)
			elif n+2 in range(0, len(all_merged_entities)) and n != 0:
				window, ud_sublist = make_ud(n-1, n+3, all_merged_entities, spacy_dic)
			elif n+1 in range(0, len(all_merged_entities)) and n != 0:
				window, ud_sublist = make_ud(n-1, n+2, all_merged_entities, spacy_dic)
			#elif: add to capture other relations
			else:
				window, ud_sublist = make_ud(n, n+1,  all_merged_entities, spacy_dic)
			
			#The list of dependency patterns that we want to capture
			options= [
			['nsubj', 'cop', 'conj', 'amod'],
			['cop', 'conj', 'amod'],
			['case', 'det', 'nmod'],
			['acl', 'case', 'nmod'],
			['nmod', 'amod'],
			['obl', 'acl'],
			['obl', 'amod'],
			['amod', 'nmod'],
			['acl', 'obl'],
			['amod', 'obl',],
			['case', 'nmod'],
			['case', 'amod'],
			]
			
			#Uncomment to see the UD relations			
			'''print('Window: ', window)
			print('Text in window: ', [spacy_dic[k[0]]['text'] for k in window])
			print('Ud_sublist: ', ud_sublist)'''
			
			#Check the ud_sublist (UD tags) to see if there are patterns that match the patterns that we want to capture in relation to the identified entity (index 1)
			query_items = []
			for i in range(0, 2): #we want to always compare in relation to the entity
				for ii in range(1, len(ud_sublist)): #...but with a larger window
					if ii-i != 0:
						match = ud_sublist[i:ii] #potential match
						for option in options:
							if match == option:
								#print("UD match! " + str(match))
								window_match = window[i:ii]
								for words in window_match:
									if spacy_dic[words[0]]['text'] not in query_items:
										query_items.append(spacy_dic[words[0]]['text'])
			
			#Add all of the relevant neighbors to the query
			if len(query_items) > 0:			
				query = ' '.join(query_items)
				if query not in queries:
					queries.append(query)
					#print('UD match, add words to query: ' + str(query))

			#If an entity doesn't have any relevant neighbors:
			if len(query_items) == 0:
				if len(window) == 1:
					if spacy_dic[window[0][0]]['text'] not in queries:
						queries.append(spacy_dic[window[0][0]]['text'])
						#print('No UD match; just add concept to query: ' + str(spacy_dic[window[0][0]]['text']))
				else:
					if spacy_dic[window[1][0]]['text'] not in queries:
						queries.append(spacy_dic[window[1][0]]['text'])
						#print('No UD match; just add concept to query: ' + str(spacy_dic[window[1][0]]['text']))

		return queries

	except Exception as ex:
		print('Error in UD: ', ex)
		return queries


'''
'''
def ud_translation():
	pass


'''
'''
def make_queries(search_query, jsonfile, entity, apis):

	with open(jsonfile, "r+", encoding='utf-8') as f:
		data = json.load(f)
		keywords = data['keyword']

	#############################
	#FR search queries#
	search_query = search_query.lower()
	keyword = keywords['fra']

	if keyword not in search_query:
		search_queries = [keyword + ', ' + search_query] #FR
	elif keyword in search_query:
		search_queries = [search_query]


	#############################
	#EN search queries#
	if search_query != entity['text'].lower():
		#means that there are UD matches to translate
		#do something to translate the extra terms
		#idea: you have the entity and you have the keyword
		#make it so that you can automatically translate the entity using babel
		#for the other parts of the search query, use WN to translate it
		#OR you can use babel net, which will have some info 
		pass


	translation = autosyn.syn_translation(entity, apis, 'eng') #only translates entity

	keyword_translation = keywords['eng']

	#Check if some terms are the same in FR and EN
	if translation in search_queries[0]:
		translation == None

	if keyword_translation and translation:
		if keyword_translation not in translation:
			search_queries.append(keyword_translation + ', ' +  translation)
		else:
			search_queries.append(translation)

	#to eliminate any duplicates for proper name searches (ie ['Texas', 'Texas'])
	if search_queries[0] == search_queries[1]:
		search_queries = [search_queries[0]]

	return search_queries




'''
Checks if entity is a named entity or a concept, and whether it's single or compound.
Accesses image-retrieving functions in autoimage.
'''
def bab_entities(spacy_dic, sentence, path_n, babel_client, apis, jsonfile):

	BAB_API_KEY = apis['babel']['key']

	entities_list= []
	
	#Babelfy sentence (str)
	babel_client.babelfy(sentence)

	for n in range(0, len(babel_client.all_merged_entities)):

		try: 
			#print('\n' + 'First step entity ' + babel_client.all_merged_entities[n]['text'])
			entity = babel_client.all_merged_entities[n]

			##Here, first off check if it's a noun or not
			noun = autosyn.check_noun(entity, apis)

			##ENTITIES
			#1. Only care about entities
			if entity['isEntity'] and noun and autosyn.check_ne(entity, apis):

				search_queries = make_queries(entity['text'], jsonfile, entity, apis)
				synset = autosyn.get_synset(entity, apis)

				#Since NEs are generally less ambiguous than concepts, we access images from wikimedia to potentially illustrate the entities. The image urls are saved, but not yet retrieved.
				imageURLs, imageFiles = autoimage.get_ne_imgs(entity, apis, path_n, 5)

				entity_dic = {'recognized': entity['text'], 'query': search_queries, 'synset': synset, 'imageFolder': path_n, 'imageURLs': imageURLs, 'imageFiles': imageFiles, 'best': []}
				entities_list.append(entity_dic)


			##CONCEPTS
			#2. If it's a concept...
			elif entity['isEntity'] and noun and not autosyn.check_ne(entity, apis):

				#2.1 If it's composed, take the concept as a whole
				if entity['tokenFragment']['start'] != entity['tokenFragment']['end']:	
					#Check if the concept should be discarded (abstract)
					check = autosyn.check_abstract(entity, apis)
					
					if check:
						search_queries = make_queries(entity['text'], jsonfile, entity, apis)
						synset = autosyn.get_synset(entity, apis)
						entity_dic = {'recognized': entity['text'], 'query': search_queries, 'synset': synset, 'imageFolder': path_n, 'imageURLs':[], 'imageFiles': [], 'best': []}
						entities_list.append(entity_dic)

					else:
						#print("Concept is an abstraction")
						pass
				
				#2.2 If not composed, check if it's worth checking and then check for modifiers.
				elif entity['tokenFragment']['start'] == entity['tokenFragment']['end']:

					#Check if the concept should be discarded (abstract)
					abstract = autosyn.check_abstract(entity, apis)

					queries = []
					if not abstract:
						all_merged_entities = babel_client.all_merged_entities
						queries = ud(n, all_merged_entities, spacy_dic, queries)			
						recognized = ', '.join(queries)

						#print('UD recognized:', recognized)
						search_queries = make_queries(recognized, jsonfile, entity, apis)
						synset = autosyn.get_synset(entity, apis)
						entity_dic = {'recognized': recognized, 'query': search_queries, 'synset': synset, 'imageFolder': path_n, 'imageURLs':[], 'imageFiles': [], 'best': []}
						entities_list.append(entity_dic)	
					
					elif abstract:
						#print("Ignore: Concept is an abstraction")
						pass

			else:
				#print('"' + babel_client.all_merged_entities[n]['text'] + '" not recognized as a concept/entity')
				pass	

		except Exception as ex:
			print('Error in bab_entities: ', ex)

	return entities_list



'''
Starter function which starts the tokenization
'''
def start(filepath, subfolder, lang, babel_client, jsonfile, apis):
	jsonfile = spacy_tok(filepath, subfolder, lang, babel_client, jsonfile, apis)

	

