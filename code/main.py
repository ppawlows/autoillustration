'''
To run:

python main.py folder/with/textfiles LANG 

for example:
python main.py textscopy/fr FR
'''

import requests, os, re, json, sys

from babelpy.babelfy import BabelfyClient
from nltk.corpus import wordnet as wn

#Local
import autokeyword
import autotext
import autosyn


if __name__ == '__main__':
	
	text_folder = sys.argv[1] #folder with texts
	apikeys = sys.argv[2] #json file
	lang = sys.argv[3] #input language (FR)
	
	#Set up Babelfy
	params = dict()
	if lang.lower():
		lang= lang.upper()
	params['lang'] = lang

	with open(apikeys, "r+", encoding='utf-8') as f:
		apis = json.load(f)
		BAB_API_KEY = apis['babel']['key']
	
	babel_client = BabelfyClient(BAB_API_KEY, params)
	
	#Iterate through texts in folder
	for file in os.listdir(text_folder):
		if file.endswith(".txt"):

			#The text file to read
			filepath = text_folder + '/' + file

			#Get the keyword. 
			#doc = autokeyword.read_docs(filepath)
			#candidates = autokeyword.candidates(doc)
			#keyword = autokeyword.ner_keywords(candidates, doc, babel_client)
			keyword = file[:-4] #Current implementation takes the user input as the keyword.

			#Create subdirectory where images will be stored
			if os.path.basename(os.path.normpath(os.getcwd())) == 'autoillustration':
				if not os.path.isdir('results'):
					os.makedirs('results', exist_ok=True)
				subfolder = os.getcwd() + '/results/' + file[:-4]
			
			elif os.path.basename(os.path.normpath(os.getcwd())) == 'code':
				components = os.getcwd().split(os.sep)
				subfolder = str.join(os.sep, components[:-1])
				os.chdirs('..')
				os.makedirs('results', exist_ok=True)
				subfolder = os.getcwd() + '/results/' + file[:-4]
				os.chdirs('code')	
			
			else:
				subfolder = os.getcwd() + '/results/' + file[:-4]

			if not os.path.isdir(subfolder):
				os.makedirs(subfolder, exist_ok=True)

			#Creating the jsonfile where the image info will be written
			towrite = {'keyword': {'fra' : keyword.lower(), 'eng': autosyn.syn_translation(keyword, apis, target_lang = 'eng')}, 'results' : subfolder, 'sentences': []}
			#TODO: translate the keyword here

			
			jsonfile = subfolder + '/' + file[:-4] + '.json'

			with open(jsonfile, "w", encoding='utf-8') as f:
				json.dump(towrite, f, indent= 3, ensure_ascii=False)
			
			image_dic = autotext.start(filepath, subfolder, lang, babel_client, jsonfile, apis)


