'''
To run:

python evaluation.py pathgeneratedjsonfile/autoillustrated/keyword/number/keyword.json
'''

import keras
from keras.applications import mobilenet
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.imagenet_utils import decode_predictions

import tensorflow as tf

import numpy as np

from nltk.corpus import wordnet as wn

import json, os, re, sys 
import urllib.request

#Local
import autoimage
import autotext
import autosyn


'''
Updates json file
'''
def update_json(jsonfile):
	with open(jsonfile, "r+", encoding='utf-8') as f:
		data= json.load(f)
		new_sentence= {'number': n, 'text': sentence, 'entities': entities_list}
		data['sentences'].append(new_sentence)
	
	with open(jsonfile, "w", encoding='utf-8') as f:
		json.dump(data, f, indent=3, ensure_ascii=False)


'''
Compares the image classification predictions with the entity that was the original search
Returns a list of the best photos (verified photos)
'''
def compare(image_predictions_dic, entity_wn_dic):

	best = []
	
	for image in image_predictions_dic:
		for synset in image_predictions_dic[image]['synsets']:

			#If synset is the same
			if synset == entity_wn_dic['synsets']:
				best.append(image)

			#If the image's syn is a synonym for the entity
			elif synset in entity_wn_dic['synonyms']:
					best.append(image)
	return best



'''
Classifies retrieved images, predicting their content used WN tags.
TODO: split function into two
'''
def clasify_images(jsonfile):
	#Load the MobileNet model
	#mobilenet_model = mobilenet.MobileNet(weights='imagenet')
	mobilenet_model = keras.applications.MobileNet()
	#Open JSON file
	with open(jsonfile, "r+", encoding='utf-8') as f:
		data = json.load(f)
		for sentence in data['sentences']:
			
			for entity in sentence['entities']:

				if entity['synset'] != None and entity['synset'] != []:

					#WN info for the word
					babel_synset = entity['synset'] #strait.s.01

					synsets = wn.synset(entity['synset'])
					
					#synonyms
					synonyms = wn.synsets(entity['recognized'], lang= 'fra')

					#hypernyms
					hyper = lambda s: s.hypernyms()
					hyper_list = list(synsets.closure(hyper))

					#hyponyms
					hypo = lambda s: s.hyponyms()
					hypo_list = list(synsets.closure(hypo))

					entity_wn_dic = {'predictions': entity['synset'], 'synsets': synsets, 'hyper': hyper_list, 'hypo': hypo_list, 'synonyms': synonyms}

					image_predictions_dic = {} #put prediction info for every image here

					for image in entity['imageFiles']:

						if image.endswith('.svg') == False and image.endswith('webm') == False:

							image_predictions_dic[image]= {'predictions': [], 'synsets': [], 'hyper': [], 'hypo': [], 'synonyms': []} 

							#Load images and convert
							original = load_img(image, target_size=(224, 224))
							numpy_image = img_to_array(original)
							image_batch = np.expand_dims(numpy_image, axis=0)

							#Predictions on photo contents
							processed_image_mobilenet = mobilenet.preprocess_input(image_batch.copy())
							predictions_mobilenet = mobilenet_model.predict(processed_image_mobilenet)
							label_mobilenet = decode_predictions(predictions_mobilenet)

							#Iterate through the predictions and create dictionary for each retrived image
							for result in label_mobilenet:
								for label in result:
									pos = label[0][0]
									
									if label[0][1] == '0':
										offset = int(label[0][2:])
									else:
										offset = int(label[0][1:])

									#synsets
									images_synset = wn.synset_from_pos_and_offset(pos, offset) #this is the spefific synset
									image_predictions_dic[image]['synsets'].append(images_synset)
									
									#Image predictions
									images_synset_name = images_synset.name()
									image_predictions_dic[image]['predictions'].append(images_synset_name)
							
									#The hyper/hyponyms
									if image_predictions_dic[image]['synsets'] != []:
										for s in image_predictions_dic[image]['synsets']:
											h1= list(s.closure(hypo))
											for h in h1:
												image_predictions_dic[image]['hypo'].append(h)
											h2 = s.closure(hyper)
											for h in h2:
												image_predictions_dic[image]['hyper'].append(h)

										#synonyms

										#wn.synsets(image_predictions_dic[image]['synsets'][0].name(), lang= 'fra')
										name = s.name()
										name = wn.synset(name).lemma_names()[0]
										image_synonyms = wn.synsets(name)
										image_predictions_dic[image]['synonyms'].append(image_synonyms)

					best = compare(image_predictions_dic, entity_wn_dic)

					entity['best'] = best
				
				else: 
					entity['best'] = []


	with open(jsonfile, "w", encoding='utf-8') as f:
		json.dump(data, f, indent=3, ensure_ascii=False)



if __name__ == '__main__':	
	jsonfile = sys.argv[1] #folder with texts
	clasify_images(jsonfile)
